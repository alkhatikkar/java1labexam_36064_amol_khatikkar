package test;

import java.util.*;

class Employee {
	private int empid;
	private String name;
	private double salary;

	public Employee() {
		super();

	}

	public Employee(int empid, String name, double salary) {
		super();
		this.empid = empid;
		this.name = name;
		this.salary = salary;
	}

	public int getEmpid() {
		return empid;
	}

	public String getName() {
		return name;
	}

	public double getSalary() {
		return salary;
	}

	public List<Employee> SortBySalary(List<Employee> empList) {

		Collections.sort(empList, new EmployeeComparator());

		return empList;
	}

}

class EmployeeComparator implements Comparator<Employee> {

	public int compare(Employee emp1, Employee emp2) {
		return (int) (emp2.getSalary() - emp1.getSalary());
	}
}

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int empCount = Integer.parseInt(in.nextLine());

		List<Employee> empList = new ArrayList<Employee>();
		while (empCount > 0) {
			int id = in.nextInt();
			String name = in.next();
			double salary = in.nextDouble();

			Employee emp = new Employee(id, name, salary);
			empList.add(emp);

			empCount--;
		}

		Employee employee = new Employee();

		List<Employee> sortedEmpList = employee.SortBySalary(empList);

		for (Employee emp : sortedEmpList) {
			System.out.println(emp.getName());
		}
	}
}
