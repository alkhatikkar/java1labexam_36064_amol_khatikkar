package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pojo.Singer;
import utils.DBUtils;

public class DaoLayer implements Closeable {
	Connection connection = null;
	Statement statement = null;

	public DaoLayer() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	// Add Singer
	public int insert(Singer Singer) throws Exception {
		String sql = "INSERT INTO Singer VALUES('" + Singer.getName() + "','" + Singer.getGender() + "',"
				+ Singer.getAge() + ",'" + Singer.getEmial_id() + "'," + Singer.getContact() + "," + Singer.getRating()
				+ ")";
		return this.statement.executeUpdate(sql);
	}

	// Modify Rating
	public int update(String name, int rating) throws Exception {
		String sql = "UPDATE Singer SET rating=" + rating + " WHERE name= " + name + "";
		return this.statement.executeUpdate(sql);
	}

	// Remove Singer
	public int delete(int contact) throws Exception {
		String sql = "DELETE FROM Singer WHERE contact=" + contact;
		return this.statement.executeUpdate(sql);
	}

	// Display Singers
	public List<Singer> getSinger() throws Exception {
		String sql = "SELECT * FROM Singer";
		List<Singer> Singer = new ArrayList<Singer>();
		try (ResultSet rs = this.statement.executeQuery(sql)) {
			while (rs.next())
				Singer.add(new Singer(rs.getString("name"), rs.getString("gender"), rs.getInt("age"),
						rs.getString("emial_id"), rs.getInt("contact"), rs.getInt("rating")));
		}
		return Singer;
	}

	// Search Singers with name from Collection
	public int Search(String name) throws SQLException {
		return 0;
	}

	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);
		}
	}
}