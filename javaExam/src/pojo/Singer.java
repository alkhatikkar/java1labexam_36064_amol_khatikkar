package pojo;

public class Singer {

	private String name;
	private String gender;
	private int age;
	private String emial_id;
	private int contact;
	private int rating;

	public Singer() {

	}

	public Singer(String name, String gender, int age, String emial_id, int contact, int rating) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.emial_id = emial_id;
		this.contact = contact;
		this.rating = rating;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmial_id() {
		return emial_id;
	}

	public void setEmial_id(String emial_id) {
		this.emial_id = emial_id;
	}

	public int getContact() {
		return contact;
	}

	public void setContact(int contact) {
		this.contact = contact;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return String.format("-10s%-10s%-10d%-15s%-10d%-10d%", name, gender, age, emial_id, contact, rating);
	}

}
