package test;

import java.util.List;
import java.util.Scanner;

import dao.DaoLayer;
import pojo.Singer;

public class Program {
	static Scanner sc = new Scanner(System.in);

	static public int menuList() {
		int choice;
		System.out
				.println("||---------------------------------Singer Applicaion-----------------------------------||\n");
		System.out.println("0. exit");
		System.out.println("1. Add Singer");
		System.out.println("2. Delete Singer");
		System.out.println("3. Modify Singer");
		System.out.println("4. Search singer with name from collection");
		System.out.println("5. Display Singer\n");
		System.out.println("Enter desired choice : ");
		choice = sc.nextInt();
		return choice;
	}

	public static void main(String[] args) {
		int choice;
		while ((choice = Program.menuList()) != 0) {
			switch (choice) {
			case 1:
				try (DaoLayer dao = new DaoLayer()) {
					Singer singer = new Singer("Amol", "male", 24, "Oshwaat@gmail.com", 65745, 10);
					int count = dao.insert(singer);
					System.out.println(count + " no. of rows affected");

				} catch (Exception e) {

					e.printStackTrace();
				}
				break;
			case 2:
				try (DaoLayer dao = new DaoLayer()) {
					int count = dao.delete(53423);
					System.out.println(count + " row(s) affected");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				break;
			case 3:
				try (DaoLayer dao = new DaoLayer()) {
					int count = dao.update("amol", 4);
					System.out.println(count + " row(s) affected");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;
			case 4:
				try (DaoLayer dao = new DaoLayer()) {
					int count = dao.Search("amol");
					System.out.println(count + " row(s) affected");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;

			case 5:
				try (DaoLayer dao = new DaoLayer()) {
					List<Singer> book = dao.getSinger();
					for (Singer singer : book) {
						System.out.println(singer.toString());
					}
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}
}